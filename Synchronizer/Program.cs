﻿using System.Globalization;
using System.Threading;
using CommandLine;

namespace Synchronizer
{
    class Program
    { 
        static void Main(string[] args)
        {
            var culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            Parser.Default.ParseArguments<Options>(args).WithParsed(DoWork);
        }

        private static void DoWork(Options opt)
        {
            var w = new Worker();
            w.DoWork(opt);
        }
    }
}