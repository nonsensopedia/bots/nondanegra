using System.Collections.Generic;
using System.IO;

namespace Synchronizer
{
    public class MetricsReader
    {
        private readonly Dictionary<string, int> _metrics = new Dictionary<string, int>();

        public MetricsReader()
        {
            using (var reader = new StreamReader("paths.tsv"))
            {
                reader.ReadLine();
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine().Split('\t');
                    if (line.Length != 2) continue;
                    
                    _metrics.Add(line[0], int.Parse(line[1]));
                }
            }
        }

        public int? GetMetrics(string title)
        {
            if (_metrics.TryGetValue(title, out int depth))
                return depth;
            return null;
        }
    }
}