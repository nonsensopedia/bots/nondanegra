﻿using System.Collections.Generic;
using Synchronizer.Analytics;
using Synchronizer.GraPage;

namespace Synchronizer
{
    public class PageSet
    {
        public IEnumerable<BluePage> BluePages => _bluePages.Values;

        public IEnumerable<RedPage> RedPages => _redPages.Values;

        private readonly Dictionary<string, RedPage> _redPages;
        private readonly Dictionary<string, BluePage> _bluePages;

        public readonly AnalyticsInterface AnalyticsInterface;

        public PageSet(AnalyticsInterface ga)
        {
            _redPages = new Dictionary<string, RedPage>();
            _bluePages = new Dictionary<string, BluePage>();

            AnalyticsInterface = ga;
        }

        public void AddBlue(BluePage ble)
        {
            _bluePages.Add(ble.Title, ble);
        }

        public bool HasBluePage(string title) => _bluePages.ContainsKey(title);
        
        public bool HasRedPage(string title) => _redPages.ContainsKey(title);
        
        public bool HasPage(string title) => _bluePages.ContainsKey(title) || _redPages.ContainsKey(title);

        public RedPage TryInsertRed(string title)
        {
            if (_redPages.TryGetValue(title, out var communist))
                return communist;

            communist = new RedPage(title, this);
            _redPages.Add(title, communist);
            return communist;
        }
    }
}
