﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using WikiClientLibrary.Generators;
using WikiClientLibrary.Pages;
using WikiClientLibrary.Sites;

namespace Synchronizer.GraPage
{
    public class BluePage : GraPage
    {
        public WikiPage OriginalPage;

        private static readonly Regex ChooseRegex = new Regex(@"<\s*choose\s*>.*?<\s*\/\s*choose\s*>",
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);

        private static readonly Regex LinkRegex = new Regex(@"(?<=\[\[)(([^]]*?(?=\|))|([^\|]*?(?=\]\])))",
            RegexOptions.Compiled);

        private static readonly Regex TemplateRegex = new Regex(@"(?<={{)(([^}]*?(?=\|))|([^\|]*?(?=}})))",
            RegexOptions.Compiled);

        private static readonly Dictionary<string, LinkType> LinkTypeMapping = new Dictionary<string, LinkType>
        {
            ["Gra:"] = LinkType.GameLink,
            ["Plik:"] = LinkType.FileLink,
            ["Grafika:"] = LinkType.FileLink,
            ["File:"] = LinkType.FileLink
        };

        private readonly RatingReader _ratingReader;
        private (int Count, double Avg) _rating = (0, 0);
        private DateTime _createdTimestamp, _lastRevisionTimestamp;
        private string _originalAuthor, _authors;
        private int _revisionCount;
        private bool _containsChoose = false;

        public BluePage(WikiPage originalPage, RatingReader ratingReader, PageSet parentSet) : base(originalPage.Title, parentSet)
        {
            OriginalPage = originalPage;
            _ratingReader = ratingReader;
        }

        public void Recreate(WikiSite site)
        {
            OriginalPage = new WikiPage(site, Title);
        }

        public void ParseOriginal(bool fetchAnalytics)
        {
            GetRevisionInfo();
            GetAnalyticsInfo();
            Links = new List<(string Link, LinkType Type)>();
            var remainingText = OriginalPage.Content;
            var ms = ChooseRegex.Matches(OriginalPage.Content);

            foreach (Match match in ms)
            {
                remainingText = remainingText.Replace(match.Value, "");
                Links.AddRange(ParseLinks(match.Value)
                    .Select(x => (x.link, x.type + 1))
                );
            }

            _containsChoose = ms.Count > 0;

            Links.AddRange(ParseLinks(remainingText));
            Links = Links.Distinct().ToList();
            Cats = ParseCats(remainingText).ToList();
            _rating = _ratingReader.GetAverageRatingAsync(OriginalPage.Id).Result;
        }

        private IEnumerable<(string link, LinkType type)> ParseLinks(string s)
        {
            foreach (Match match in LinkRegex.Matches(s))
            {
                var m = match.Value.NormalizePagetitle();
                if (m == "") continue;

                foreach (var pair in LinkTypeMapping)
                    if (m.StartsWith(pair.Key))
                    {
                        var type = pair.Value;
                        lock (ParentSet) if (type == LinkType.GameLink && !ParentSet.HasBluePage(m))
                        {
                            ParentSet.TryInsertRed(m);
                            type = LinkType.Redlink;
                        }

                        yield return (m.Substring(pair.Key.Length), type);
                        break;
                    }
                        
            }

            foreach (Match match in TemplateRegex.Matches(s))
            {
                var m = match.Value.NormalizePagetitle();

                if (!m.StartsWith("Gra:"))
                    continue;

                yield return (m.Substring(4), LinkType.Transclusion);
            }
        }

        //meow
        private IEnumerable<string> ParseCats(string s)
        {
            return CatRegex.Matches(s).Select(x => x.Value);
        }

        private void GetRevisionInfo()
        {
            var gen = OriginalPage.CreateRevisionsGenerator();
            gen.PaginationSize = 500;

            var revs = gen.EnumItemsAsync().ToEnumerable().ToArray();
            _createdTimestamp = revs.Last().TimeStamp;
            _lastRevisionTimestamp = revs.First().TimeStamp;
            _originalAuthor = revs.Last().UserName;
            _authors = revs
                .Select(x => x.UserName)
                .Distinct()
                .OrderBy(x => x)
                .Aggregate(new StringBuilder(), (builder, s) => builder.Append(s).Append("#"))
                .ToString();
            _revisionCount = revs.Length;
        }

        protected override IEnumerable<(string Key, string Value)> GetAdditionalKeys()
        {
            yield return ("istnieje=", "true");

            foreach (var kvp in TemplateParamMapping)
            {
                var agg = Links
                    .Where(x => x.Type == kvp.Key)
                    .Aggregate("", (s, p) => s + p.Link + "#");
                if (agg.Length > 1)
                    yield return (kvp.Value, agg);
            }

            string avgtext = "", ctext = "";
            if (!double.IsNaN(_rating.Avg) && _rating.Count > 0)
            {
                avgtext = $"{_rating.Avg:F4}".Replace('.', ',');    //to jest diablo brzydki hack ale nie chce mi się poprawiać
                ctext = $"{_rating.Count}";
            }

            yield return ("średnia=", avgtext);
            yield return ("ilośćocen=", ctext);
            yield return ("datautw=", _createdTimestamp.ToString("yyyy-MM-dd HH:mm:ss"));
            yield return ("datamod=", _lastRevisionTimestamp.ToString("yyyy-MM-dd HH:mm:ss"));
            yield return ("wersji=", _revisionCount.ToString());
            yield return ("pierwszyautor=", _originalAuthor);
            yield return ("autorzy=", _authors);
            if (_containsChoose) yield return ("zawierachoose=", "prawda");
        }
    }
}
