﻿using System.Collections.Generic;

namespace Synchronizer.GraPage
{
    public class RedPage : GraPage
    {
        public RedPage(string title, PageSet parentSet) : base(title, parentSet)
        {
            var cat = title;
            var tl = title.ToLower();
            if (tl.Contains("strona"))
                cat = cat.Substring(tl.IndexOf("strona") + 6).Trim();
            else cat = cat.Substring(cat.IndexOf(':') + 1).Trim();

            string pageType;
            if (title.Contains("Strona ")) pageType = "tryb klasyczny";
            else pageType = "inne";

            Cats = new List<string> { $"[[Kategoria:Gra – redlinki ({pageType})|{cat}]]" };
        }

        protected override IEnumerable<(string Key, string Value)> GetAdditionalKeys()
        {
            yield return ("istnieje=", "false");
        }
    }
}
