using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Synchronizer.Analytics;
using WikiClientLibrary.Pages;
using WikiClientLibrary.Sites;

namespace Synchronizer.GraPage
{
    public enum LinkType
    {
        GameLink = 1,
        RandomGameLink = 2,
        Transclusion = 3,
        RandomTransclusion = 4,
        FileLink = 5,
        RandomFileLink = 6,
        Redlink = 7,
        RandomRedlink = 8
    }
    
    public abstract class GraPage
    {
        public readonly string Title;

        protected static readonly Regex CatRegex = new Regex(@"\[\[(kategoria|category):.*?]]",
            RegexOptions.Compiled | RegexOptions.IgnoreCase);

        protected static readonly Regex NewlineRegex = new Regex(@"(?<!}})(\n *){2,}", RegexOptions.Compiled);

        protected static readonly Regex WhitespaceOnLineEndRegex = new Regex(@" +\n", RegexOptions.Compiled);

        protected static readonly Regex GraRegex = new Regex(@"{{gra.*?\n}}",
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);
        
        protected static readonly Regex PipeRegex = new Regex(@"(?:\[\[[^]]*?]][^[]*?)*(\|)",
            RegexOptions.Compiled | RegexOptions.Singleline);

        protected static readonly Dictionary<LinkType, string> TemplateParamMapping = new Dictionary<LinkType, string>
        {
            [LinkType.GameLink] = "linkuje=",
            [LinkType.RandomGameLink] = "linkujelos=",
            [LinkType.Transclusion] = "transkluduje=",
            [LinkType.RandomTransclusion] = "transkludujelos=",
            [LinkType.FileLink] = "linkujepliki=",
            [LinkType.RandomFileLink] = "linkujeplikilos=",
            [LinkType.Redlink] = "redlinkuje=",
            [LinkType.RandomRedlink] = "redlinkujelos="
        };
        
        protected static readonly string[] OtherAllowedParams = new []
        {
            "typy=",
            "wątki=",
            "motywy=",
            "konwergencja=",
            "znaczniki=",
            "opis="
        };

        protected List<(string Link, LinkType Type)> Links;
        protected List<string> Cats;
        protected readonly PageSet ParentSet;
        protected int Views, UniqueViews;
        protected float ExitRate, AvgTime;
        protected IEnumerable<LinkInfo> LinksInfo;

        protected GraPage(string title, PageSet parentSet)
        {
            Title = title;
            ParentSet = parentSet;
        }

        public bool UpdateNondane(WikiSite nondane, bool dryRun = false)
        {
            var page = new WikiPage(nondane, Title);
            page.RefreshAsync(PageQueryOptions.FetchContent).Wait();

            var text = page.Content ?? "";
            
            if (text.Count(x => x == '[') != 
                text.Count(x => x == ']'))
            {
                Console.WriteLine($"Jakiś funny business jest ze stroną {page.Title}");
                return false;
            }

            //szabloneu
            List<string> split = new List<string>();
            if (!text.ToLower().Contains("{{gra"))
                text = "{{Gra\n}}" + text;
            else
            {
                var template = GraRegex.Match(text).Value;
                var stringSplit = new List<string>();
                int lastIndex = 0;
                
                foreach (Match match in PipeRegex.Matches(template))
                {
                    var ix = match.Groups[1].Index;
                    stringSplit.Add(template.Substring(
                        lastIndex,
                        ix - lastIndex
                    ));
                    lastIndex = ix + 1;
                }
                stringSplit.Add(template.Substring(lastIndex));
                
                split.AddRange(
                stringSplit
                    .Skip(1)
                    .Where(x => TemplateParamMapping.All(kvp => !x.Contains(kvp.Value)))
                    .Select(x => x.Trim())
                );
                if (split.Count > 0 && split[split.Count - 1].EndsWith("}}"))
                    split[split.Count - 1] = split[split.Count - 1]
                        .Substring(0, split[split.Count - 1].Length - 2);
            }

            //więcej parametrów!
            var keyVals = GetAdditionalKeys().ToList();
            if (Views > 0)
            {
                keyVals.Add(("odwiedziny=", Views.ToString()));
                keyVals.Add(("uni_odwiedziny=", UniqueViews.ToString()));
                keyVals.Add(("odsetek_wyjść=", ExitRate.ToString("F4").Replace('.', ',')));
                keyVals.Add(("średni_czas=", AvgTime.ToString("F4").Replace('.', ',')));
                
                int it = 1;
                foreach (var linkInfo in LinksInfo.Take(100))
                {
                    keyVals.Add(($"s_w_z{it}=", linkInfo.Page));
                    keyVals.Add(($"s_w_l{it}=", linkInfo.Clicks.ToString()));
                    it++;
                }
            }

            //dodajemy/aktualizujemy parametry
            foreach (var (key, txt) in keyVals)
            {
                var ix = split.FindIndex(x => x.StartsWith(key));
                if (ix > -1)
                {
                    if (txt != "") split[ix] = key + txt;
                    else split.RemoveAt(ix);
                }
                else if (txt != "") split.Add(key + txt);
            }
            
            //wywalamy niedozwolone parametry
            split = split.Where(x =>
            {
                var key = x.Split('=')[0].Trim().ToLower() + "=";
                return OtherAllowedParams.Contains(key) || keyVals.Any(y => y.Key == key);
            }).ToList();
            
            //wypluwamy gotowy szablon
            var sb = new StringBuilder("{{Gra\n");
            foreach (var s in split)
                sb.Append("|").Append(s).Append("\n");
            sb.Append("}}");

            text = GraRegex.Replace(text, sb.ToString());

            //kategorie
            var katy = CatRegex.Matches(text).Select(x => x.Value).ToArray();
            foreach (var cat in katy.Except(Cats))
                text = text.Replace(cat, "");
            foreach (var cat in Cats.Except(katy))
                text += $"\n{cat}";
            
            //sprzątanko
            text = WhitespaceOnLineEndRegex.Replace(text, "\n");
            text = NewlineRegex.Replace(text, "\n");

            if (page.Content == text) return false;
            
            page.Content = text;
            if (!dryRun) page.UpdateContentAsync("Automagiczne informacje", false, true).Wait();
            return true;
        }

        protected abstract IEnumerable<(string Key, string Value)> GetAdditionalKeys();
        
        protected void GetAnalyticsInfo()
        {
            var (links, pageStats) = ParentSet.AnalyticsInterface.GetData(Title);
            LinksInfo = links;
            Views = pageStats.Views;
            UniqueViews = pageStats.UniqueViews;
            ExitRate = pageStats.ExitRate;
            AvgTime = pageStats.AvgTime;
        }
    }
}