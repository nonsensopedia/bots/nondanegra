using CommandLine;

namespace Synchronizer
{
    public class Options
    {
        [Option("dry-run", Default = false, HelpText = "Wykonuje wszystkie operacje, ale nie zapisuje zmian.")]
        public bool DryRun { get; set; }
        
        [Option("action", Required = true, HelpText = "Akcja do wykonania. Jedno z: 'main', 'metrics'.")]
        public string Action { get; set; }
    }
}