﻿using System.Text.RegularExpressions;
using System.Web;

namespace Synchronizer
{
    public static class Utils
    {
        private static readonly Regex TemplateParamRegex = new Regex(@"{{{.*?}}}",
            RegexOptions.Compiled);

        private static readonly Regex MultipleWhitespaceRegex = new Regex(@"\s{2,}", RegexOptions.Compiled);

        public static string NormalizePagetitle(this string s)
        {
            if (string.IsNullOrWhiteSpace(s)) return "";
            if (s.Contains("#")) return "";

            s = s.Replace("+", "%2B");      //bo mediawiki robi po swojemu
            s = TemplateParamRegex.Replace(HttpUtility.UrlDecode(s), "");
            s = s.Replace('_', ' ').Trim();
            s = MultipleWhitespaceRegex.Replace(s, " ");

            while (s.EndsWith('/'))
                s = s.Substring(0, s.Length - 1);

            var ix = s.IndexOf(':');
            if (ix < 0) return s.CapitalizeFirst();

            var ns = s.Substring(0, ix).Trim();
            var title = s.Substring(ix + 1).Trim();

            return $"{ns.CapitalizeFirst()}:{title.CapitalizeFirst()}";
        }

        private static string CapitalizeFirst(this string s)
        {
            if (string.IsNullOrWhiteSpace(s)) return s;
            if (s.Length == 1) return s.ToUpper();
            return s[0].ToString().ToUpper() + s.Substring(1);
        }
    }
}
