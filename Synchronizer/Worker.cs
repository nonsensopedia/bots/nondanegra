using System;
using System.Linq;
using System.Text.RegularExpressions;
using Synchronizer.Analytics;
using Synchronizer.GraPage;
using WikiClientLibrary.Client;
using WikiClientLibrary.Generators;
using WikiClientLibrary.Infrastructures;
using WikiClientLibrary.Pages;

namespace Synchronizer
{
    public class Worker
    {
        private readonly NonsaWikiSite _siteNonsa;
        private readonly NonsaWikiSite _siteNondane;

        public Worker()
        {
            Console.WriteLine("Łączę...");
            var clientN = new WikiClient
            {
                ClientUserAgent = "NondaneGra"
            };
            var clientD = new WikiClient
            {
                ClientUserAgent = "NondaneGra"
            };

            var throttler = new Throttler
            {
                ThrottleTime = TimeSpan.FromSeconds(1.0)
            };

            var (l, p) = LoginLoader.GetLoginPass("login.txt");
            _siteNonsa = new NonsaWikiSite(clientN, "https://nonsa.pl/api.php", l, p);
            _siteNondane = new NonsaWikiSite(clientD, "https://dane.nonsa.pl/api.php", l, p)
                { ModificationThrottler = throttler };

            try
            {
                _siteNonsa.Initialization.Wait();
                _siteNondane.Initialization.Wait();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Nie mogę się połączyć z Nonsą :'(\n" + ex.Message);
            }
            Console.WriteLine("Połączono.");
        }
        
        public void DoWork(Options opt)
        {
            if (opt.DryRun) Console.WriteLine("Dry run!");

            var action = opt.Action.Trim();
            switch (action)
            {
                case "main":
                    ActionMain(opt);
                    break;
                case "metrics":
                    ActionMetrics(opt);
                    break;
                default:
                    Console.WriteLine($"Nieznana akcja: '{action}'");
                    break;
            }

            Console.WriteLine("Wykonane.");
        }

        private void ActionMain(Options opt)
        {
            var gen = new AllPagesGenerator(_siteNonsa)
            {
                NamespaceId = 108,
                PaginationSize = 500
            };

            var rr = new RatingReader(@"https://nonsa.pl/api.php");

            var ga = new AnalyticsInterface();
            var pages = gen.EnumPagesAsync().ToEnumerable();
            var pageSet = new PageSet(ga);

            int c = 0;
            Console.WriteLine("Układam se manatki...");
            foreach (var p in pages)
            {
                if (c % 1000 == 0) Console.WriteLine(c);

                var gp = new BluePage(p, rr, pageSet);
                pageSet.AddBlue(gp);

                c++;
            }

            Console.WriteLine("Robię mlem...");
            foreach (var p in pageSet.BluePages)
            {
                int tries = 0;

                do
                {
                    tries++;
                    try
                    {
                        p.OriginalPage.RefreshAsync(PageQueryOptions.FetchContent).Wait();
                        p.ParseOriginal(true);
                        var changed = p.UpdateNondane(_siteNondane, opt.DryRun);
                        Console.WriteLine($"{(changed ? "OKEJ" : "SKIP")} {p.Title}");
                        break;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"ERROR {p.Title}");
                        Console.WriteLine(ex.Message);
                        Console.WriteLine(ex.StackTrace);
                    }
                } while (tries <= 5);
            }

            Console.WriteLine("A teraz redlinki :3");
            foreach (var p in pageSet.RedPages)
            {
                int tries = 0;

                do
                {
                    tries++;
                    try
                    {
                        var changed = p.UpdateNondane(_siteNondane, opt.DryRun);
                        Console.WriteLine($"{(changed ? "OKEJ" : "SKIP")} {p.Title}");
                        break;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"ERROR {p.Title}");
                        Console.WriteLine(ex.Message);
                        Console.WriteLine(ex.StackTrace);
                    }
                } while (tries <= 5);
            }
            
            Console.WriteLine("Kasuję niepotrzebne strony w ND");
            var allInNdGen = new AllPagesGenerator(_siteNondane)
            {
                NamespaceId = 9800,
                PaginationSize = 500
            };

            allInNdGen.EnumPagesAsync().ForEachAsync(p =>
            {
                if (!pageSet.HasPage(p.Title))
                {
                    p.DeleteAsync("niepotrzebna strona – brak odpowiadającej strony na Nonsie").Wait();
                    Console.WriteLine($"Skasowano niepotrzebną stronę: {p.Title}");
                }
            }).Wait();
        }

        private void ActionMetrics(Options opt)
        {
            var statsRegex = new Regex(@"\|\s*glebokosc\s*=\s*\d*\s", RegexOptions.Singleline);
            var templateStartRegex = new Regex(@"{{\s*gra", RegexOptions.IgnoreCase);
            
            var metricsReader = new MetricsReader();
            var allInNdGen = new AllPagesGenerator(_siteNondane)
            {
                NamespaceId = 9800,
                PaginationSize = 500
            };
            allInNdGen.EnumPagesAsync(PageQueryOptions.FetchContent).ForEachAsync(p =>
            {
                var match = statsRegex.Match(p.Content);
                var metrics = metricsReader.GetMetrics(p.Title);

                var newMetrics = "";
                if (!(metrics is null)) newMetrics = $"|glebokosc={metrics}";

                if (!match.Success)
                {
                    if (newMetrics == "")
                    {
                        Console.WriteLine($"SKIP {p.Title}");
                        return;
                    }
                    p.Content = templateStartRegex.Replace(
                        p.Content,
                m => $"{m.Value}\n{newMetrics}"
                    );
                    Console.WriteLine($"INSERT {p.Title}");
                }
                else
                {
                    var newContent = p.Content.Replace(match.Value, newMetrics + "\n");
                    if (newMetrics == "") Console.WriteLine($"REMOVE {p.Title}");
                    else if (newContent != p.Content) Console.WriteLine($"UPDATE {p.Title}");
                    else
                    {
                        Console.WriteLine($"SKIP {p.Title}");
                        return;
                    }

                    p.Content = newContent;
                }

                if (!opt.DryRun)
                    p.UpdateContentAsync("aktualizuję metryki", true, true).Wait();
            }).Wait();
        }
    }
}