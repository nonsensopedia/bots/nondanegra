namespace Synchronizer.Analytics
{
    public struct PageStats
    {
        public int Views;
        public int UniqueViews;
        public float ExitRate;
        public float AvgTime;
    }
}