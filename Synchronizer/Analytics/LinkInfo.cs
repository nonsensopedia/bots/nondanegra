namespace Synchronizer.Analytics
{
    public class LinkInfo
    {
        public string Page;
        public int Clicks;
    }
}