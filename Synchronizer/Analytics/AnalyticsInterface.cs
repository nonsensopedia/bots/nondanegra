using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using Google.Apis.AnalyticsReporting.v4;
using Google.Apis.AnalyticsReporting.v4.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Util.Store;

namespace Synchronizer.Analytics
{
    public class AnalyticsInterface
    {
        private readonly AnalyticsReportingService _service;

        private const string ViewId = "191218205";
        
        public AnalyticsInterface()
        {
            UserCredential credential;
            using (var stream = new FileStream("client_secret.json", FileMode.Open, FileAccess.Read))
            {
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    new[] { AnalyticsReportingService.Scope.AnalyticsReadonly },
                    "user", CancellationToken.None, new FileDataStore("Analytics.Tokens")
                    ).Result;
            }
            
            _service = new AnalyticsReportingService(new BaseClientService.Initializer
            {
                ApplicationName = "Nondane Gra",
                HttpClientInitializer = credential
            });
        }

        public (IEnumerable<LinkInfo> Outlinks, PageStats Stats) GetData(string title)
        {
            var dateRange = new DateRange
            {
                StartDate = "2019-01-01",
                EndDate = "Today"
            };
            
            var views = new Metric
            {
                Expression = "ga:pageviews",
                Alias = "views"
            };

            var uniqueViews = new Metric
            {
                Expression = "ga:uniquePageviews",
                Alias = "uniqueViews"
            };

            var exitRate = new Metric
            {
                Expression = "ga:exitRate",
                Alias = "exitRate"
            };
            
            var avgTimeOnPage = new Metric
            {
                Expression = "ga:avgTimeOnPage",
                Alias = "avgTimeOnPage"
            };

            var dim = new Dimension
            {
                Name = "ga:pageTitle"
            };
            var dim2 = new Dimension
            {
                Name = "ga:previousPagePath"
            };

            var filter = new DimensionFilter
            {
                CaseSensitive = true,
                DimensionName = "ga:pageTitle",
                Expressions = new [] {"^" + Regex.Escape(title) + " – Nonsensopedia"}
            };

            var request = new ReportRequest
            {
                DateRanges = new [] {dateRange},
                Dimensions = new [] {dim, dim2},
                Metrics = new [] {views, uniqueViews, exitRate, avgTimeOnPage},
                DimensionFilterClauses = new []
                {
                    new DimensionFilterClause{Filters = new [] {filter}}
                },
                ViewId = ViewId,
                SamplingLevel = "LARGE"
            };
            var getRequest = new GetReportsRequest
            {
                ReportRequests = new [] {request}
            };

            var response = _service.Reports.BatchGet(getRequest).Execute();

            Dictionary<string, LinkInfo> linkInfos = new Dictionary<string, LinkInfo>();

            if (response.Reports[0].Data?.Rows == null)
            {
                return (
                    new LinkInfo[0],
                    new PageStats()
                );
            }

            foreach (var row in response.Reports[0].Data.Rows)
            {
                var page = ParseUrl(row.Dimensions[1]);

                if (linkInfos.TryGetValue(page, out var linkInfo))
                    linkInfo.Clicks += int.Parse(row.Metrics[0].Values[0]);
                else linkInfos.Add(page, new LinkInfo
                {
                    Clicks = int.Parse(row.Metrics[0].Values[0]),
                    Page = page
                });
            }

            var vals = response.Reports[0].Data.Totals[0].Values;

            return (
                linkInfos.Values.OrderByDescending(x => x.Clicks), 
                new PageStats
                {
                    Views = int.Parse(vals[0]),
                    UniqueViews = int.Parse(vals[1]),
                    ExitRate = float.Parse(vals[2]),
                    AvgTime = float.Parse(vals[3])
                });
        }

        private string ParseUrl(string url)
        {
            if (url == "(entrance)")
                return "(wejście)";
            if (!url.StartsWith("/wiki/Gra:"))
                return "(inne)";

            return url.Substring(6).Split("?")[0].Replace("_", " ");
        }
    }
}