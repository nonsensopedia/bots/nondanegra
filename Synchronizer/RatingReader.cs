using System;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Synchronizer
{
    public class RatingReader
    {
        private static readonly Regex RatingRegex = new Regex("(?<=\\d\":)\\d*", RegexOptions.Compiled);
        private readonly HttpClient _client;

        public RatingReader(string apiUrl)
        {
            _client = new HttpClient
            {
                BaseAddress = new Uri(apiUrl)
            };
        }
        
        public async Task<(int Count, double Average)> GetAverageRatingAsync(int pageId)
        {
            try
            {
                var result = await _client.GetAsync(@"?action=ratepage&format=json&pageid=" + pageId);
                var text = await result.Content.ReadAsStringAsync();

                int sum = 0; 
                double sumMultiplied = 0;
                double multiplier = 1;
                foreach (Match match in RatingRegex.Matches(text))
                {
                    var i = int.Parse(match.Value);
                    sum += i;
                    sumMultiplied += i * multiplier;
                    multiplier++;
                }
            
                return (sum, sumMultiplied / sum);
            }
            catch
            {
                return (0, 0);
            }
        }
    }
}