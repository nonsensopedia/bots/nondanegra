using System;
using System.IO;

namespace Synchronizer
{
    public static class LoginLoader
    {
        public static (string login, string pass) GetLoginPass(string file)
        {
            if (!File.Exists(file))
            {
                File.Create(file);
                Console.WriteLine($"Nie znalazłem pliku z loginem i hasłem więc ci taki plik zrobiłem: {file}, " +
                                  "powinien być obok binarki.\n" +
                                  "W pierwszej linijce wpisz login, w drugiej hasło.");
            }

            string l = "", p = "";
            using (var f = new StreamReader(file))
            {
                l = f.ReadLine().Trim();
                p = f.ReadLine().Trim();
            }

            return (l, p);
        }
    }
}